import { NavLink } from 'react-router-dom';
import './index.css'

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/" style={{ marginRight: '100px' }}>KittyCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <div className="btn-group">
          <button type="button" className="btn btn-outline-light dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" style={{ marginRight: '75px' }}>
              Automobile Inventory
            </button>
            <ul className="dropdown-menu">
              <li>
                <NavLink className="dropdown-item" to="manufacturers/create">Create a Manufacturer</NavLink>
                <NavLink className="dropdown-item" to="models/create">Create a Model</NavLink>
                <NavLink className="dropdown-item" to="automobiles/new">Add an Automobile</NavLink>
                <NavLink className="dropdown-item" to="manufacturers/list">Manufacturers</NavLink>
                <NavLink className="dropdown-item" to="models/list">Models</NavLink>
                <NavLink className="dropdown-item" to="automobiles/list">Automobiles</NavLink>
              </li>
            </ul>
          </div>
          <div className="btn-group">
            <button type="button" className="btn btn-outline-light dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" style={{ marginRight: '75px' }}>
              Salespeople
            </button>
            <ul className="dropdown-menu">
              <li>
                <NavLink className="dropdown-item" to="salespeople/new">Add a Salesperson</NavLink>
                <NavLink className="dropdown-item" to="salespeople/list">Salespeople</NavLink>
                <NavLink className="dropdown-item" to="salespeople/history">Salesperson History</NavLink>
              </li>
            </ul>
          </div>
          <div className="btn-group">
            <button type="button" className="btn btn-outline-light dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" style={{ marginRight: '75px' }}>
              Sales
            </button>
            <ul className="dropdown-menu">
              <li>
                <NavLink className="dropdown-item" to="sales/new">Add a Sale</NavLink>
                <NavLink className="dropdown-item" to="sales/list">Sales</NavLink>
              </li>
            </ul>
          </div>
          <div className="btn-group">
            <button type="button" className="btn btn-outline-light dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" style={{ marginRight: '75px' }}>
              Customers
            </button>
            <ul className="dropdown-menu">
              <li>
                <NavLink className="dropdown-item" to="customers/new">Add a Customer</NavLink>
                <NavLink className="dropdown-item" to="customers/list">Customers</NavLink>
              </li>
              </ul>
          </div>
          <div className="btn-group">
            <button type="button" className="btn btn-outline-light dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" style={{ marginRight: '75px' }}>
              Technicians
            </button>
            <ul className="dropdown-menu">
              <li>
                <NavLink className="dropdown-item" aria-current="page" to="technicians/create">Add a Technician</NavLink>
                <NavLink className="dropdown-item" aria-current="page" to="technicians/list">Technicians</NavLink>
              </li>
            </ul>
          </div>
          <div className="btn-group">
            <button type="button" className="btn btn-outline-light dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" style={{ marginRight: '75px' }}>
              Service Appointments
            </button>
            <ul className="dropdown-menu">
              <li>
                <NavLink className="dropdown-item" aria-current="page" to="appointments/create">Create a Service Appointment</NavLink>
                <NavLink className="dropdown-item" aria-current="page" to="appointments/list">Service Appointments</NavLink>
                <NavLink className="dropdown-item" aria-current="page" to="appointments/history">Service History</NavLink>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>
  )
}

export default Nav;

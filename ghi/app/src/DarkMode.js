import React, { useState, useEffect } from 'react';
import './DarkMode.css';

function DarkMode() {
    const [theme, setTheme] = useState(
        localStorage.getItem("theme") || "light"
    );
    const toggleTheme = () => {
        if (theme === "light") {
            setTheme("dark");
        } else {
            setTheme("light");
        }
    };
    useEffect(() => {
        localStorage.setItem("theme", theme);
        document.body.className = theme;
    }, [theme]);

    return (
        <div className={'App ${theme}'}>
            <div className={`tdnn ${theme === "light" ? "day" : ""}`} onClick={toggleTheme}>
                <div className={theme === 'light' ? 'sun' : 'moon'}></div>
            </div>
        </div>
    );
};

export default DarkMode;

import React, {useEffect, useState} from 'react';

function AutomobileList() {
    const [automobiles, setAutomobiles] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8100/api/automobiles/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos);
        };
    };

    useEffect(() => {
        fetchData();
    },[]);

    const deleteAutomobile = async (vin) => {
        const url = `http://localhost:8100/api/automobiles/${vin}/`;
        const response = await fetch(url, { method: 'DELETE' });
        if (response.ok) {
            setAutomobiles(automobiles.filter(automobile => automobile.vin !== vin));
        } else {
            console.error('Failed to delete an automobile');
        }
    };

    return (
        <div>
            <h2 style={{ marginTop: "20px" }}>Automobiles</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                        <th>Sold</th>
                    </tr>
                </thead>
                <tbody>
                    {automobiles.map((automobile) => {
                        return (
                            <tr key={automobile.id} value={automobile.id}>
                                <td>{automobile.vin}</td>
                                <td>{automobile.color}</td>
                                <td>{automobile.year}</td>
                                <td>{automobile.model.name}</td>
                                <td>{automobile.model.manufacturer.name}</td>
                                <td>{automobile.sold ? "yes":"no"}</td>
                                <td>
                                    <button type="button" className="btn btn-outline-secondary btn-sm button-style" onClick={() => deleteAutomobile(automobile.vin)}>Delete</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
};

export default AutomobileList;

import React, {useEffect, useState} from 'react';

function CustomerList() {
    const [customer, setCustomer] = useState([])

    const fetchData = async () => {
        const url = "http://localhost:8090/api/customers/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setCustomer(data.customer)
        }
    };

    useEffect(() => {
        fetchData();
    },[]);

    const deleteCustomer = async (id) => {
        const url = `http://localhost:8090/api/customers/${id}/`;
        const response = await fetch(url, { method: 'DELETE' });
        if (response.ok) {
            setCustomer(customer.filter(customer => customer.id !== id));
        } else {
            console.error('Failed to delete a customer');
        }
    };

    return (
        <div>
            <h2 style={{ marginTop: "20px" }}>Customers</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Address</th>
                        <th>Phone Number</th>
                    </tr>
                </thead>
                <tbody>
                    {customer.map((cust) => {
                        return (
                            <tr key={cust.id} value={cust.id}>
                                <td>{cust.first_name}</td>
                                <td>{cust.last_name}</td>
                                <td>{cust.address}</td>
                                <td>{cust.phone_number}</td>
                                <td>
                                    <button type="button" className="btn btn-outline-secondary btn-sm button-style" onClick={() => deleteCustomer(cust.id)}>Delete</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default CustomerList;

import React, {useEffect, useState} from 'react';

function SalespersonList() {
    const [salesperson, setSalesPerson] = useState([])

    const fetchData = async () => {
        const url = "http://localhost:8090/api/salespeople/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalesPerson(data.salesperson)
        };
    };

    useEffect(() => {
        fetchData();
    },[]);

    const deleteSalesperson = async (id) => {
        const url = `http://localhost:8090/api/salespeople/${id}/`;
        const response = await fetch(url, { method: 'DELETE' });
        if (response.ok) {
            setSalesPerson(salesperson.filter(salespers => salespers.id !== id));
        } else {
            console.error('Failed to delete a salesperson');
        }
    };

    return (
        <div>
            <h2 style={{ marginTop: "20px" }}>Salespeople</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Employee ID</th>
                    </tr>
                </thead>
                <tbody>
                    {salesperson.map((salespers) => {
                        return (
                            <tr key={salespers.id} value={salespers.id}>
                                <td>{salespers.first_name}</td>
                                <td>{salespers.last_name}</td>
                                <td>{salespers.employee_id}</td>
                                <td>
                                    <button type="button" className="btn btn-outline-secondary btn-sm button-style" onClick={() => deleteSalesperson(salespers.id)}>Delete</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
};

export default SalespersonList;

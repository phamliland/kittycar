import React from 'react';
import './index.css'; 
import carImage2 from './pusheen2.png'

function AnimatedCar() {
return (
    <div className="car-container">
        <img
        className="car-image"
        src={carImage2}
        alt="Animated car"
        />
    </div>
    );
}

export default AnimatedCar;

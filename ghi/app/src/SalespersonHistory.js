import React, {useEffect, useState} from 'react';

function SalespersonHistory() {
    const [salespeople, setSalespeople] = useState([]);
    const [sales, setSales] = useState([]);
    const [salesperson, setSalesperson] = useState('');

    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    };

    const fetchData = async () => {
        const salesUrl = "http://localhost:8090/api/sales/";
        const salespeopleUrl = "http://localhost:8090/api/salespeople/";
        const salesResponse = await fetch(salesUrl);
        const salespeopleResponse = await fetch(salespeopleUrl);

        if (salespeopleResponse.ok && salesResponse.ok) {
            const salespeopleData = await salespeopleResponse.json();
            const salesData = await salesResponse.json();
            setSalespeople(salespeopleData.salesperson);
            setSales(salesData.sale);
        };
    };

    const saleFilter = () => {
        const filteredSale = sales.filter(sale => sale.salesperson.employee_id === salesperson);
        return filteredSale;
    };

    useEffect(() => {
        fetchData();
    },[]);

    const deleteSales = async (id) => {
        const url = `http://localhost:8090/api/sales/${id}/`;
        const response = await fetch(url, { method: 'DELETE' });
        if (response.ok) {
            setSales(sales.filter(sale => sale.id !== id));
        } else {
            console.error('Failed to delete a sale');
        }
    };

    return (
        <div>
            <h2 style={{ marginTop: "20px" }}>Salesperson History</h2>
            <label htmlFor="select a salesperson">Select a Salesperson</label>
            <select id="select a salesperson" value={salesperson} onChange={handleSalespersonChange}>
            <option value="">Select a Salesperson</option>
            {salespeople.map((salesperson) => (
                <option key={salesperson.employee_id} value={salesperson.employee_id}>
                    {salesperson.first_name} {salesperson.last_name}
                </option>
            ))}
            </select>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {saleFilter().map((sale) => {
                        return (
                            <tr key={sale.id} value={sale.id}>
                                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                                <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                                <td>{sale.automobile.vin}</td>
                                <td>{sale.price}</td>
                                <td>
                                    <button type="button" className="btn btn-outline-secondary btn-sm button-style" onClick={() => deleteSales(sale.id)}>Delete</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
};

export default SalespersonHistory;

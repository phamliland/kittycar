from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .encoders import TechnicianEncoder, AppointmentEncoder
from .models import Technician, Appointment


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=400
            )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_technician(request, pk):
    if request.method == "GET":
        technician = Technician.objects.get(pk=pk)
        return JsonResponse(
            {"technician": technician},
            encoder=TechnicianEncoder
        )
    elif request.method == "DELETE":
        count, _ = Technician.objects.filter(pk=pk).delete()
        return JsonResponse({"deleted": count > 0}, status=200)
    else:
        try:
            content = json.loads(request.body)
            Technician.objects.filter(pk=pk).update(**content)
            technician = Technician.objects.get(pk=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=400
            )


@require_http_methods(["GET", "POST"])
def api_list_appointment(request, technician_pk=None):
    if request.method == "GET":
        if technician_pk is not None:
            appointments = Appointment.objects.filter(technician=technician_pk)
        else:
            appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            technician_id = content["technician"]
            technician = Technician.objects.get(pk=technician_id)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid tech"},
                status=400
            )
        appointment = Appointment.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_appointment(request, pk):
    if request.method == "GET":
        appointment = Appointment.objects.get(pk=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(pk=pk).delete()
        return JsonResponse({"deleted": count > 0}, status=200)
    else:
        content = json.loads(request.body)
        try:
            if "technician" in content:
                technician_id = content["technician"]
                technician = Technician.objects.get(pk=technician_id)
                content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid tech"},
                status=400
            )
        Appointment.objects.filter(pk=pk).update(**content)
        appointment = Appointment.objects.get(pk=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )


@require_http_methods(["PUT"])
def api_canceled_appointment(request, pk):
    appointment = Appointment.objects.get(pk=pk)
    appointment.canceled()
    return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def api_finished_appointment(request, pk):
    appointment = Appointment.objects.get(pk=pk)
    appointment.finished()
    return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False,
    )

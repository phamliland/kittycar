from .models import Salesperson, Customer, Sale, AutomobileVO
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from .encoders import CustomerEncoder, SalesPersonEncoder, SaleEncoder
import json


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse(
            {'customer': customer},
            encoder=CustomerEncoder
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            {'customer': customer},
            encoder=CustomerEncoder,
            safe=False,
        )
    else:
        return JsonResponse(
            {'message': 'Customer not found'},
            status=400,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_customer_details(request, pk):
    if request.method == "GET":
        customer = Customer.objects.get(pk=pk)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Customer.objects.filter(pk=pk).delete()
        return JsonResponse({'deleted': count > 0})
    else:
        content = json.loads(request.body)
        Customer.objects.filter(pk=pk).update(**content)
        customer = Customer.objects.get(pk=pk)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salesperson = Salesperson.objects.all()
        return JsonResponse(
            {'salesperson': salesperson},
            encoder=SalesPersonEncoder
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            {'salesperson': salesperson},
            encoder=SalesPersonEncoder,
            safe=False,
        )
    else:
        return JsonResponse(
            {'message': 'Salesperson not found'},
            status=400,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_salespeople_details(request, pk):
    if request.method == "GET":
        salesperson = Salesperson.objects.get(pk=pk)
        return JsonResponse(
            salesperson,
            encoder=SalesPersonEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Salesperson.objects.filter(pk=pk).delete()
        return JsonResponse({'deleted': count > 0})
    else:
        content = json.loads(request.body)
        Salesperson.objects.filter(pk=pk).update(**content)
        salesperson = Salesperson.objects.get(pk=pk)
        return JsonResponse(
            salesperson,
            encoder=SalesPersonEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sale = Sale.objects.all()
        return JsonResponse(
            {'sale': sale},
            encoder=SaleEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            salesperson = Salesperson.objects.get(id=content['salesperson'])
            content['salesperson'] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {'message': 'Salesperson does not exist'},
                status=400,
            )
        try:
            customer = Customer.objects.get(id=content['customer'])
            content['customer'] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {'message': 'Customer does not exist'},
                status=400,
            )
        try:
            vin = content['automobile']
            automobile = AutomobileVO.objects.get(vin=vin)
            content['automobile'] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {'message': 'Automobile does not exist'},
                status=400,
            )
        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_sale_details(request, pk):
    if request.method == "GET":
        sale = Sale.objects.get(pk=pk)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Sale.objects.filter(pk=pk).delete()
        return JsonResponse({'deleted': count > 0})
    else:
        content = json.loads(request.body)
        Sale.objects.filter(pk=pk).update(**content)
        sale = Sale.objects.get(pk=pk)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )

from django.db import models


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=100)
    import_href = models.CharField(max_length=100)

    def __str__(self):
        return self.vin


class Salesperson(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.employee_id


class Customer(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    address = models.CharField(max_length=150)
    phone_number = models.CharField(max_length=20, unique=True)

    def __str__(self):
        return self.phone_number


class Sale(models.Model):
    price = models.CharField(max_length=50)
    salesperson = models.ForeignKey(
        Salesperson,
        related_name='sale',
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        Customer,
        related_name='sale',
        on_delete=models.CASCADE,
    )
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name='sale',
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.customer
